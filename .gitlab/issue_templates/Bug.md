Reporting a bug that is intrinsically related to siesta-bundle

NOTE: Please select option `Library Bug` when reporting bugs specific to a library

- [ ] Ensure the title is prefixed with `[bundle] `
- [ ] Add a description of the problem encountered with siesta-bundle
- [ ] Describe how the bundle was built
- [ ] Preferably attach your JHbuild `config.rc`
- [ ] If you have a fix, please let us know! :-)
